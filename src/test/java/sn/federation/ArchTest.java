package sn.federation;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {

        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("sn.federation");

        noClasses()
            .that()
                .resideInAnyPackage("sn.federation.service..")
            .or()
                .resideInAnyPackage("sn.federation.repository..")
            .should().dependOnClassesThat()
                .resideInAnyPackage("..sn.federation.web..")
        .because("Services and repositories should not depend on web layer")
        .check(importedClasses);
    }
}
