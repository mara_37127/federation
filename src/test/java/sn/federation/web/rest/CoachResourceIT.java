package sn.federation.web.rest;

import sn.federation.FederationApp;
import sn.federation.domain.Coach;
import sn.federation.repository.CoachRepository;
import sn.federation.service.CoachService;
import sn.federation.service.dto.CoachCriteria;
import sn.federation.service.CoachQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CoachResource} REST controller.
 */
@SpringBootTest(classes = FederationApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CoachResourceIT {

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    private static final String DEFAULT_PRENOM = "AAAAAAAAAA";
    private static final String UPDATED_PRENOM = "BBBBBBBBBB";

    private static final Integer DEFAULT_ANCIENETE = 1;
    private static final Integer UPDATED_ANCIENETE = 2;
    private static final Integer SMALLER_ANCIENETE = 1 - 1;

    @Autowired
    private CoachRepository coachRepository;

    @Autowired
    private CoachService coachService;

    @Autowired
    private CoachQueryService coachQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCoachMockMvc;

    private Coach coach;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Coach createEntity(EntityManager em) {
        Coach coach = new Coach()
            .nom(DEFAULT_NOM)
            .prenom(DEFAULT_PRENOM)
            .ancienete(DEFAULT_ANCIENETE);
        return coach;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Coach createUpdatedEntity(EntityManager em) {
        Coach coach = new Coach()
            .nom(UPDATED_NOM)
            .prenom(UPDATED_PRENOM)
            .ancienete(UPDATED_ANCIENETE);
        return coach;
    }

    @BeforeEach
    public void initTest() {
        coach = createEntity(em);
    }

    @Test
    @Transactional
    public void createCoach() throws Exception {
        int databaseSizeBeforeCreate = coachRepository.findAll().size();
        // Create the Coach
        restCoachMockMvc.perform(post("/api/coaches")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(coach)))
            .andExpect(status().isCreated());

        // Validate the Coach in the database
        List<Coach> coachList = coachRepository.findAll();
        assertThat(coachList).hasSize(databaseSizeBeforeCreate + 1);
        Coach testCoach = coachList.get(coachList.size() - 1);
        assertThat(testCoach.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testCoach.getPrenom()).isEqualTo(DEFAULT_PRENOM);
        assertThat(testCoach.getAncienete()).isEqualTo(DEFAULT_ANCIENETE);
    }

    @Test
    @Transactional
    public void createCoachWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = coachRepository.findAll().size();

        // Create the Coach with an existing ID
        coach.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCoachMockMvc.perform(post("/api/coaches")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(coach)))
            .andExpect(status().isBadRequest());

        // Validate the Coach in the database
        List<Coach> coachList = coachRepository.findAll();
        assertThat(coachList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllCoaches() throws Exception {
        // Initialize the database
        coachRepository.saveAndFlush(coach);

        // Get all the coachList
        restCoachMockMvc.perform(get("/api/coaches?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(coach.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].prenom").value(hasItem(DEFAULT_PRENOM)))
            .andExpect(jsonPath("$.[*].ancienete").value(hasItem(DEFAULT_ANCIENETE)));
    }
    
    @Test
    @Transactional
    public void getCoach() throws Exception {
        // Initialize the database
        coachRepository.saveAndFlush(coach);

        // Get the coach
        restCoachMockMvc.perform(get("/api/coaches/{id}", coach.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(coach.getId().intValue()))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM))
            .andExpect(jsonPath("$.prenom").value(DEFAULT_PRENOM))
            .andExpect(jsonPath("$.ancienete").value(DEFAULT_ANCIENETE));
    }


    @Test
    @Transactional
    public void getCoachesByIdFiltering() throws Exception {
        // Initialize the database
        coachRepository.saveAndFlush(coach);

        Long id = coach.getId();

        defaultCoachShouldBeFound("id.equals=" + id);
        defaultCoachShouldNotBeFound("id.notEquals=" + id);

        defaultCoachShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultCoachShouldNotBeFound("id.greaterThan=" + id);

        defaultCoachShouldBeFound("id.lessThanOrEqual=" + id);
        defaultCoachShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllCoachesByNomIsEqualToSomething() throws Exception {
        // Initialize the database
        coachRepository.saveAndFlush(coach);

        // Get all the coachList where nom equals to DEFAULT_NOM
        defaultCoachShouldBeFound("nom.equals=" + DEFAULT_NOM);

        // Get all the coachList where nom equals to UPDATED_NOM
        defaultCoachShouldNotBeFound("nom.equals=" + UPDATED_NOM);
    }

    @Test
    @Transactional
    public void getAllCoachesByNomIsNotEqualToSomething() throws Exception {
        // Initialize the database
        coachRepository.saveAndFlush(coach);

        // Get all the coachList where nom not equals to DEFAULT_NOM
        defaultCoachShouldNotBeFound("nom.notEquals=" + DEFAULT_NOM);

        // Get all the coachList where nom not equals to UPDATED_NOM
        defaultCoachShouldBeFound("nom.notEquals=" + UPDATED_NOM);
    }

    @Test
    @Transactional
    public void getAllCoachesByNomIsInShouldWork() throws Exception {
        // Initialize the database
        coachRepository.saveAndFlush(coach);

        // Get all the coachList where nom in DEFAULT_NOM or UPDATED_NOM
        defaultCoachShouldBeFound("nom.in=" + DEFAULT_NOM + "," + UPDATED_NOM);

        // Get all the coachList where nom equals to UPDATED_NOM
        defaultCoachShouldNotBeFound("nom.in=" + UPDATED_NOM);
    }

    @Test
    @Transactional
    public void getAllCoachesByNomIsNullOrNotNull() throws Exception {
        // Initialize the database
        coachRepository.saveAndFlush(coach);

        // Get all the coachList where nom is not null
        defaultCoachShouldBeFound("nom.specified=true");

        // Get all the coachList where nom is null
        defaultCoachShouldNotBeFound("nom.specified=false");
    }
                @Test
    @Transactional
    public void getAllCoachesByNomContainsSomething() throws Exception {
        // Initialize the database
        coachRepository.saveAndFlush(coach);

        // Get all the coachList where nom contains DEFAULT_NOM
        defaultCoachShouldBeFound("nom.contains=" + DEFAULT_NOM);

        // Get all the coachList where nom contains UPDATED_NOM
        defaultCoachShouldNotBeFound("nom.contains=" + UPDATED_NOM);
    }

    @Test
    @Transactional
    public void getAllCoachesByNomNotContainsSomething() throws Exception {
        // Initialize the database
        coachRepository.saveAndFlush(coach);

        // Get all the coachList where nom does not contain DEFAULT_NOM
        defaultCoachShouldNotBeFound("nom.doesNotContain=" + DEFAULT_NOM);

        // Get all the coachList where nom does not contain UPDATED_NOM
        defaultCoachShouldBeFound("nom.doesNotContain=" + UPDATED_NOM);
    }


    @Test
    @Transactional
    public void getAllCoachesByPrenomIsEqualToSomething() throws Exception {
        // Initialize the database
        coachRepository.saveAndFlush(coach);

        // Get all the coachList where prenom equals to DEFAULT_PRENOM
        defaultCoachShouldBeFound("prenom.equals=" + DEFAULT_PRENOM);

        // Get all the coachList where prenom equals to UPDATED_PRENOM
        defaultCoachShouldNotBeFound("prenom.equals=" + UPDATED_PRENOM);
    }

    @Test
    @Transactional
    public void getAllCoachesByPrenomIsNotEqualToSomething() throws Exception {
        // Initialize the database
        coachRepository.saveAndFlush(coach);

        // Get all the coachList where prenom not equals to DEFAULT_PRENOM
        defaultCoachShouldNotBeFound("prenom.notEquals=" + DEFAULT_PRENOM);

        // Get all the coachList where prenom not equals to UPDATED_PRENOM
        defaultCoachShouldBeFound("prenom.notEquals=" + UPDATED_PRENOM);
    }

    @Test
    @Transactional
    public void getAllCoachesByPrenomIsInShouldWork() throws Exception {
        // Initialize the database
        coachRepository.saveAndFlush(coach);

        // Get all the coachList where prenom in DEFAULT_PRENOM or UPDATED_PRENOM
        defaultCoachShouldBeFound("prenom.in=" + DEFAULT_PRENOM + "," + UPDATED_PRENOM);

        // Get all the coachList where prenom equals to UPDATED_PRENOM
        defaultCoachShouldNotBeFound("prenom.in=" + UPDATED_PRENOM);
    }

    @Test
    @Transactional
    public void getAllCoachesByPrenomIsNullOrNotNull() throws Exception {
        // Initialize the database
        coachRepository.saveAndFlush(coach);

        // Get all the coachList where prenom is not null
        defaultCoachShouldBeFound("prenom.specified=true");

        // Get all the coachList where prenom is null
        defaultCoachShouldNotBeFound("prenom.specified=false");
    }
                @Test
    @Transactional
    public void getAllCoachesByPrenomContainsSomething() throws Exception {
        // Initialize the database
        coachRepository.saveAndFlush(coach);

        // Get all the coachList where prenom contains DEFAULT_PRENOM
        defaultCoachShouldBeFound("prenom.contains=" + DEFAULT_PRENOM);

        // Get all the coachList where prenom contains UPDATED_PRENOM
        defaultCoachShouldNotBeFound("prenom.contains=" + UPDATED_PRENOM);
    }

    @Test
    @Transactional
    public void getAllCoachesByPrenomNotContainsSomething() throws Exception {
        // Initialize the database
        coachRepository.saveAndFlush(coach);

        // Get all the coachList where prenom does not contain DEFAULT_PRENOM
        defaultCoachShouldNotBeFound("prenom.doesNotContain=" + DEFAULT_PRENOM);

        // Get all the coachList where prenom does not contain UPDATED_PRENOM
        defaultCoachShouldBeFound("prenom.doesNotContain=" + UPDATED_PRENOM);
    }


    @Test
    @Transactional
    public void getAllCoachesByAncieneteIsEqualToSomething() throws Exception {
        // Initialize the database
        coachRepository.saveAndFlush(coach);

        // Get all the coachList where ancienete equals to DEFAULT_ANCIENETE
        defaultCoachShouldBeFound("ancienete.equals=" + DEFAULT_ANCIENETE);

        // Get all the coachList where ancienete equals to UPDATED_ANCIENETE
        defaultCoachShouldNotBeFound("ancienete.equals=" + UPDATED_ANCIENETE);
    }

    @Test
    @Transactional
    public void getAllCoachesByAncieneteIsNotEqualToSomething() throws Exception {
        // Initialize the database
        coachRepository.saveAndFlush(coach);

        // Get all the coachList where ancienete not equals to DEFAULT_ANCIENETE
        defaultCoachShouldNotBeFound("ancienete.notEquals=" + DEFAULT_ANCIENETE);

        // Get all the coachList where ancienete not equals to UPDATED_ANCIENETE
        defaultCoachShouldBeFound("ancienete.notEquals=" + UPDATED_ANCIENETE);
    }

    @Test
    @Transactional
    public void getAllCoachesByAncieneteIsInShouldWork() throws Exception {
        // Initialize the database
        coachRepository.saveAndFlush(coach);

        // Get all the coachList where ancienete in DEFAULT_ANCIENETE or UPDATED_ANCIENETE
        defaultCoachShouldBeFound("ancienete.in=" + DEFAULT_ANCIENETE + "," + UPDATED_ANCIENETE);

        // Get all the coachList where ancienete equals to UPDATED_ANCIENETE
        defaultCoachShouldNotBeFound("ancienete.in=" + UPDATED_ANCIENETE);
    }

    @Test
    @Transactional
    public void getAllCoachesByAncieneteIsNullOrNotNull() throws Exception {
        // Initialize the database
        coachRepository.saveAndFlush(coach);

        // Get all the coachList where ancienete is not null
        defaultCoachShouldBeFound("ancienete.specified=true");

        // Get all the coachList where ancienete is null
        defaultCoachShouldNotBeFound("ancienete.specified=false");
    }

    @Test
    @Transactional
    public void getAllCoachesByAncieneteIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        coachRepository.saveAndFlush(coach);

        // Get all the coachList where ancienete is greater than or equal to DEFAULT_ANCIENETE
        defaultCoachShouldBeFound("ancienete.greaterThanOrEqual=" + DEFAULT_ANCIENETE);

        // Get all the coachList where ancienete is greater than or equal to UPDATED_ANCIENETE
        defaultCoachShouldNotBeFound("ancienete.greaterThanOrEqual=" + UPDATED_ANCIENETE);
    }

    @Test
    @Transactional
    public void getAllCoachesByAncieneteIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        coachRepository.saveAndFlush(coach);

        // Get all the coachList where ancienete is less than or equal to DEFAULT_ANCIENETE
        defaultCoachShouldBeFound("ancienete.lessThanOrEqual=" + DEFAULT_ANCIENETE);

        // Get all the coachList where ancienete is less than or equal to SMALLER_ANCIENETE
        defaultCoachShouldNotBeFound("ancienete.lessThanOrEqual=" + SMALLER_ANCIENETE);
    }

    @Test
    @Transactional
    public void getAllCoachesByAncieneteIsLessThanSomething() throws Exception {
        // Initialize the database
        coachRepository.saveAndFlush(coach);

        // Get all the coachList where ancienete is less than DEFAULT_ANCIENETE
        defaultCoachShouldNotBeFound("ancienete.lessThan=" + DEFAULT_ANCIENETE);

        // Get all the coachList where ancienete is less than UPDATED_ANCIENETE
        defaultCoachShouldBeFound("ancienete.lessThan=" + UPDATED_ANCIENETE);
    }

    @Test
    @Transactional
    public void getAllCoachesByAncieneteIsGreaterThanSomething() throws Exception {
        // Initialize the database
        coachRepository.saveAndFlush(coach);

        // Get all the coachList where ancienete is greater than DEFAULT_ANCIENETE
        defaultCoachShouldNotBeFound("ancienete.greaterThan=" + DEFAULT_ANCIENETE);

        // Get all the coachList where ancienete is greater than SMALLER_ANCIENETE
        defaultCoachShouldBeFound("ancienete.greaterThan=" + SMALLER_ANCIENETE);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCoachShouldBeFound(String filter) throws Exception {
        restCoachMockMvc.perform(get("/api/coaches?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(coach.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].prenom").value(hasItem(DEFAULT_PRENOM)))
            .andExpect(jsonPath("$.[*].ancienete").value(hasItem(DEFAULT_ANCIENETE)));

        // Check, that the count call also returns 1
        restCoachMockMvc.perform(get("/api/coaches/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCoachShouldNotBeFound(String filter) throws Exception {
        restCoachMockMvc.perform(get("/api/coaches?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCoachMockMvc.perform(get("/api/coaches/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingCoach() throws Exception {
        // Get the coach
        restCoachMockMvc.perform(get("/api/coaches/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCoach() throws Exception {
        // Initialize the database
        coachService.save(coach);

        int databaseSizeBeforeUpdate = coachRepository.findAll().size();

        // Update the coach
        Coach updatedCoach = coachRepository.findById(coach.getId()).get();
        // Disconnect from session so that the updates on updatedCoach are not directly saved in db
        em.detach(updatedCoach);
        updatedCoach
            .nom(UPDATED_NOM)
            .prenom(UPDATED_PRENOM)
            .ancienete(UPDATED_ANCIENETE);

        restCoachMockMvc.perform(put("/api/coaches")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedCoach)))
            .andExpect(status().isOk());

        // Validate the Coach in the database
        List<Coach> coachList = coachRepository.findAll();
        assertThat(coachList).hasSize(databaseSizeBeforeUpdate);
        Coach testCoach = coachList.get(coachList.size() - 1);
        assertThat(testCoach.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testCoach.getPrenom()).isEqualTo(UPDATED_PRENOM);
        assertThat(testCoach.getAncienete()).isEqualTo(UPDATED_ANCIENETE);
    }

    @Test
    @Transactional
    public void updateNonExistingCoach() throws Exception {
        int databaseSizeBeforeUpdate = coachRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCoachMockMvc.perform(put("/api/coaches")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(coach)))
            .andExpect(status().isBadRequest());

        // Validate the Coach in the database
        List<Coach> coachList = coachRepository.findAll();
        assertThat(coachList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCoach() throws Exception {
        // Initialize the database
        coachService.save(coach);

        int databaseSizeBeforeDelete = coachRepository.findAll().size();

        // Delete the coach
        restCoachMockMvc.perform(delete("/api/coaches/{id}", coach.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Coach> coachList = coachRepository.findAll();
        assertThat(coachList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
