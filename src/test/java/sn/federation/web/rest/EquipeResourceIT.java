package sn.federation.web.rest;

import sn.federation.FederationApp;
import sn.federation.domain.Equipe;
import sn.federation.domain.Coach;
import sn.federation.domain.Joueur;
import sn.federation.repository.EquipeRepository;
import sn.federation.service.EquipeService;
import sn.federation.service.dto.EquipeCriteria;
import sn.federation.service.EquipeQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link EquipeResource} REST controller.
 */
@SpringBootTest(classes = FederationApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class EquipeResourceIT {

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE_CREATION = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_CREATION = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATE_CREATION = LocalDate.ofEpochDay(-1L);

    @Autowired
    private EquipeRepository equipeRepository;

    @Autowired
    private EquipeService equipeService;

    @Autowired
    private EquipeQueryService equipeQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restEquipeMockMvc;

    private Equipe equipe;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Equipe createEntity(EntityManager em) {
        Equipe equipe = new Equipe()
            .nom(DEFAULT_NOM)
            .dateCreation(DEFAULT_DATE_CREATION);
        return equipe;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Equipe createUpdatedEntity(EntityManager em) {
        Equipe equipe = new Equipe()
            .nom(UPDATED_NOM)
            .dateCreation(UPDATED_DATE_CREATION);
        return equipe;
    }

    @BeforeEach
    public void initTest() {
        equipe = createEntity(em);
    }

    @Test
    @Transactional
    public void createEquipe() throws Exception {
        int databaseSizeBeforeCreate = equipeRepository.findAll().size();
        // Create the Equipe
        restEquipeMockMvc.perform(post("/api/equipes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(equipe)))
            .andExpect(status().isCreated());

        // Validate the Equipe in the database
        List<Equipe> equipeList = equipeRepository.findAll();
        assertThat(equipeList).hasSize(databaseSizeBeforeCreate + 1);
        Equipe testEquipe = equipeList.get(equipeList.size() - 1);
        assertThat(testEquipe.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testEquipe.getDateCreation()).isEqualTo(DEFAULT_DATE_CREATION);
    }

    @Test
    @Transactional
    public void createEquipeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = equipeRepository.findAll().size();

        // Create the Equipe with an existing ID
        equipe.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEquipeMockMvc.perform(post("/api/equipes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(equipe)))
            .andExpect(status().isBadRequest());

        // Validate the Equipe in the database
        List<Equipe> equipeList = equipeRepository.findAll();
        assertThat(equipeList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllEquipes() throws Exception {
        // Initialize the database
        equipeRepository.saveAndFlush(equipe);

        // Get all the equipeList
        restEquipeMockMvc.perform(get("/api/equipes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(equipe.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].dateCreation").value(hasItem(DEFAULT_DATE_CREATION.toString())));
    }
    
    @Test
    @Transactional
    public void getEquipe() throws Exception {
        // Initialize the database
        equipeRepository.saveAndFlush(equipe);

        // Get the equipe
        restEquipeMockMvc.perform(get("/api/equipes/{id}", equipe.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(equipe.getId().intValue()))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM))
            .andExpect(jsonPath("$.dateCreation").value(DEFAULT_DATE_CREATION.toString()));
    }


    @Test
    @Transactional
    public void getEquipesByIdFiltering() throws Exception {
        // Initialize the database
        equipeRepository.saveAndFlush(equipe);

        Long id = equipe.getId();

        defaultEquipeShouldBeFound("id.equals=" + id);
        defaultEquipeShouldNotBeFound("id.notEquals=" + id);

        defaultEquipeShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultEquipeShouldNotBeFound("id.greaterThan=" + id);

        defaultEquipeShouldBeFound("id.lessThanOrEqual=" + id);
        defaultEquipeShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllEquipesByNomIsEqualToSomething() throws Exception {
        // Initialize the database
        equipeRepository.saveAndFlush(equipe);

        // Get all the equipeList where nom equals to DEFAULT_NOM
        defaultEquipeShouldBeFound("nom.equals=" + DEFAULT_NOM);

        // Get all the equipeList where nom equals to UPDATED_NOM
        defaultEquipeShouldNotBeFound("nom.equals=" + UPDATED_NOM);
    }

    @Test
    @Transactional
    public void getAllEquipesByNomIsNotEqualToSomething() throws Exception {
        // Initialize the database
        equipeRepository.saveAndFlush(equipe);

        // Get all the equipeList where nom not equals to DEFAULT_NOM
        defaultEquipeShouldNotBeFound("nom.notEquals=" + DEFAULT_NOM);

        // Get all the equipeList where nom not equals to UPDATED_NOM
        defaultEquipeShouldBeFound("nom.notEquals=" + UPDATED_NOM);
    }

    @Test
    @Transactional
    public void getAllEquipesByNomIsInShouldWork() throws Exception {
        // Initialize the database
        equipeRepository.saveAndFlush(equipe);

        // Get all the equipeList where nom in DEFAULT_NOM or UPDATED_NOM
        defaultEquipeShouldBeFound("nom.in=" + DEFAULT_NOM + "," + UPDATED_NOM);

        // Get all the equipeList where nom equals to UPDATED_NOM
        defaultEquipeShouldNotBeFound("nom.in=" + UPDATED_NOM);
    }

    @Test
    @Transactional
    public void getAllEquipesByNomIsNullOrNotNull() throws Exception {
        // Initialize the database
        equipeRepository.saveAndFlush(equipe);

        // Get all the equipeList where nom is not null
        defaultEquipeShouldBeFound("nom.specified=true");

        // Get all the equipeList where nom is null
        defaultEquipeShouldNotBeFound("nom.specified=false");
    }
                @Test
    @Transactional
    public void getAllEquipesByNomContainsSomething() throws Exception {
        // Initialize the database
        equipeRepository.saveAndFlush(equipe);

        // Get all the equipeList where nom contains DEFAULT_NOM
        defaultEquipeShouldBeFound("nom.contains=" + DEFAULT_NOM);

        // Get all the equipeList where nom contains UPDATED_NOM
        defaultEquipeShouldNotBeFound("nom.contains=" + UPDATED_NOM);
    }

    @Test
    @Transactional
    public void getAllEquipesByNomNotContainsSomething() throws Exception {
        // Initialize the database
        equipeRepository.saveAndFlush(equipe);

        // Get all the equipeList where nom does not contain DEFAULT_NOM
        defaultEquipeShouldNotBeFound("nom.doesNotContain=" + DEFAULT_NOM);

        // Get all the equipeList where nom does not contain UPDATED_NOM
        defaultEquipeShouldBeFound("nom.doesNotContain=" + UPDATED_NOM);
    }


    @Test
    @Transactional
    public void getAllEquipesByDateCreationIsEqualToSomething() throws Exception {
        // Initialize the database
        equipeRepository.saveAndFlush(equipe);

        // Get all the equipeList where dateCreation equals to DEFAULT_DATE_CREATION
        defaultEquipeShouldBeFound("dateCreation.equals=" + DEFAULT_DATE_CREATION);

        // Get all the equipeList where dateCreation equals to UPDATED_DATE_CREATION
        defaultEquipeShouldNotBeFound("dateCreation.equals=" + UPDATED_DATE_CREATION);
    }

    @Test
    @Transactional
    public void getAllEquipesByDateCreationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        equipeRepository.saveAndFlush(equipe);

        // Get all the equipeList where dateCreation not equals to DEFAULT_DATE_CREATION
        defaultEquipeShouldNotBeFound("dateCreation.notEquals=" + DEFAULT_DATE_CREATION);

        // Get all the equipeList where dateCreation not equals to UPDATED_DATE_CREATION
        defaultEquipeShouldBeFound("dateCreation.notEquals=" + UPDATED_DATE_CREATION);
    }

    @Test
    @Transactional
    public void getAllEquipesByDateCreationIsInShouldWork() throws Exception {
        // Initialize the database
        equipeRepository.saveAndFlush(equipe);

        // Get all the equipeList where dateCreation in DEFAULT_DATE_CREATION or UPDATED_DATE_CREATION
        defaultEquipeShouldBeFound("dateCreation.in=" + DEFAULT_DATE_CREATION + "," + UPDATED_DATE_CREATION);

        // Get all the equipeList where dateCreation equals to UPDATED_DATE_CREATION
        defaultEquipeShouldNotBeFound("dateCreation.in=" + UPDATED_DATE_CREATION);
    }

    @Test
    @Transactional
    public void getAllEquipesByDateCreationIsNullOrNotNull() throws Exception {
        // Initialize the database
        equipeRepository.saveAndFlush(equipe);

        // Get all the equipeList where dateCreation is not null
        defaultEquipeShouldBeFound("dateCreation.specified=true");

        // Get all the equipeList where dateCreation is null
        defaultEquipeShouldNotBeFound("dateCreation.specified=false");
    }

    @Test
    @Transactional
    public void getAllEquipesByDateCreationIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        equipeRepository.saveAndFlush(equipe);

        // Get all the equipeList where dateCreation is greater than or equal to DEFAULT_DATE_CREATION
        defaultEquipeShouldBeFound("dateCreation.greaterThanOrEqual=" + DEFAULT_DATE_CREATION);

        // Get all the equipeList where dateCreation is greater than or equal to UPDATED_DATE_CREATION
        defaultEquipeShouldNotBeFound("dateCreation.greaterThanOrEqual=" + UPDATED_DATE_CREATION);
    }

    @Test
    @Transactional
    public void getAllEquipesByDateCreationIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        equipeRepository.saveAndFlush(equipe);

        // Get all the equipeList where dateCreation is less than or equal to DEFAULT_DATE_CREATION
        defaultEquipeShouldBeFound("dateCreation.lessThanOrEqual=" + DEFAULT_DATE_CREATION);

        // Get all the equipeList where dateCreation is less than or equal to SMALLER_DATE_CREATION
        defaultEquipeShouldNotBeFound("dateCreation.lessThanOrEqual=" + SMALLER_DATE_CREATION);
    }

    @Test
    @Transactional
    public void getAllEquipesByDateCreationIsLessThanSomething() throws Exception {
        // Initialize the database
        equipeRepository.saveAndFlush(equipe);

        // Get all the equipeList where dateCreation is less than DEFAULT_DATE_CREATION
        defaultEquipeShouldNotBeFound("dateCreation.lessThan=" + DEFAULT_DATE_CREATION);

        // Get all the equipeList where dateCreation is less than UPDATED_DATE_CREATION
        defaultEquipeShouldBeFound("dateCreation.lessThan=" + UPDATED_DATE_CREATION);
    }

    @Test
    @Transactional
    public void getAllEquipesByDateCreationIsGreaterThanSomething() throws Exception {
        // Initialize the database
        equipeRepository.saveAndFlush(equipe);

        // Get all the equipeList where dateCreation is greater than DEFAULT_DATE_CREATION
        defaultEquipeShouldNotBeFound("dateCreation.greaterThan=" + DEFAULT_DATE_CREATION);

        // Get all the equipeList where dateCreation is greater than SMALLER_DATE_CREATION
        defaultEquipeShouldBeFound("dateCreation.greaterThan=" + SMALLER_DATE_CREATION);
    }


    @Test
    @Transactional
    public void getAllEquipesByCoachIsEqualToSomething() throws Exception {
        // Initialize the database
        equipeRepository.saveAndFlush(equipe);
        Coach coach = CoachResourceIT.createEntity(em);
        em.persist(coach);
        em.flush();
        equipe.setCoach(coach);
        equipeRepository.saveAndFlush(equipe);
        Long coachId = coach.getId();

        // Get all the equipeList where coach equals to coachId
        defaultEquipeShouldBeFound("coachId.equals=" + coachId);

        // Get all the equipeList where coach equals to coachId + 1
        defaultEquipeShouldNotBeFound("coachId.equals=" + (coachId + 1));
    }


    @Test
    @Transactional
    public void getAllEquipesByJoueurIsEqualToSomething() throws Exception {
        // Initialize the database
        equipeRepository.saveAndFlush(equipe);
        Joueur joueur = JoueurResourceIT.createEntity(em);
        em.persist(joueur);
        em.flush();
        equipe.addJoueur(joueur);
        equipeRepository.saveAndFlush(equipe);
        Long joueurId = joueur.getId();

        // Get all the equipeList where joueur equals to joueurId
        defaultEquipeShouldBeFound("joueurId.equals=" + joueurId);

        // Get all the equipeList where joueur equals to joueurId + 1
        defaultEquipeShouldNotBeFound("joueurId.equals=" + (joueurId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultEquipeShouldBeFound(String filter) throws Exception {
        restEquipeMockMvc.perform(get("/api/equipes?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(equipe.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].dateCreation").value(hasItem(DEFAULT_DATE_CREATION.toString())));

        // Check, that the count call also returns 1
        restEquipeMockMvc.perform(get("/api/equipes/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultEquipeShouldNotBeFound(String filter) throws Exception {
        restEquipeMockMvc.perform(get("/api/equipes?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restEquipeMockMvc.perform(get("/api/equipes/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingEquipe() throws Exception {
        // Get the equipe
        restEquipeMockMvc.perform(get("/api/equipes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEquipe() throws Exception {
        // Initialize the database
        equipeService.save(equipe);

        int databaseSizeBeforeUpdate = equipeRepository.findAll().size();

        // Update the equipe
        Equipe updatedEquipe = equipeRepository.findById(equipe.getId()).get();
        // Disconnect from session so that the updates on updatedEquipe are not directly saved in db
        em.detach(updatedEquipe);
        updatedEquipe
            .nom(UPDATED_NOM)
            .dateCreation(UPDATED_DATE_CREATION);

        restEquipeMockMvc.perform(put("/api/equipes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedEquipe)))
            .andExpect(status().isOk());

        // Validate the Equipe in the database
        List<Equipe> equipeList = equipeRepository.findAll();
        assertThat(equipeList).hasSize(databaseSizeBeforeUpdate);
        Equipe testEquipe = equipeList.get(equipeList.size() - 1);
        assertThat(testEquipe.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testEquipe.getDateCreation()).isEqualTo(UPDATED_DATE_CREATION);
    }

    @Test
    @Transactional
    public void updateNonExistingEquipe() throws Exception {
        int databaseSizeBeforeUpdate = equipeRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEquipeMockMvc.perform(put("/api/equipes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(equipe)))
            .andExpect(status().isBadRequest());

        // Validate the Equipe in the database
        List<Equipe> equipeList = equipeRepository.findAll();
        assertThat(equipeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteEquipe() throws Exception {
        // Initialize the database
        equipeService.save(equipe);

        int databaseSizeBeforeDelete = equipeRepository.findAll().size();

        // Delete the equipe
        restEquipeMockMvc.perform(delete("/api/equipes/{id}", equipe.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Equipe> equipeList = equipeRepository.findAll();
        assertThat(equipeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
