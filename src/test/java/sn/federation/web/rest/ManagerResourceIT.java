package sn.federation.web.rest;

import sn.federation.FederationApp;
import sn.federation.domain.Manager;
import sn.federation.domain.User;
import sn.federation.repository.ManagerRepository;
import sn.federation.service.ManagerService;
import sn.federation.service.dto.ManagerCriteria;
import sn.federation.service.ManagerQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ManagerResource} REST controller.
 */
@SpringBootTest(classes = FederationApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ManagerResourceIT {

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    private static final String DEFAULT_PRENOM = "AAAAAAAAAA";
    private static final String UPDATED_PRENOM = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_PASSWORD = "AAAAAAAAAA";
    private static final String UPDATED_PASSWORD = "BBBBBBBBBB";

    @Autowired
    private ManagerRepository managerRepository;

    @Autowired
    private ManagerService managerService;

    @Autowired
    private ManagerQueryService managerQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restManagerMockMvc;

    private Manager manager;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Manager createEntity(EntityManager em) {
        Manager manager = new Manager()
            .nom(DEFAULT_NOM)
            .prenom(DEFAULT_PRENOM)
            .email(DEFAULT_EMAIL)
            .password(DEFAULT_PASSWORD);
        return manager;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Manager createUpdatedEntity(EntityManager em) {
        Manager manager = new Manager()
            .nom(UPDATED_NOM)
            .prenom(UPDATED_PRENOM)
            .email(UPDATED_EMAIL)
            .password(UPDATED_PASSWORD);
        return manager;
    }

    @BeforeEach
    public void initTest() {
        manager = createEntity(em);
    }

    @Test
    @Transactional
    public void createManager() throws Exception {
        int databaseSizeBeforeCreate = managerRepository.findAll().size();
        // Create the Manager
        restManagerMockMvc.perform(post("/api/managers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(manager)))
            .andExpect(status().isCreated());

        // Validate the Manager in the database
        List<Manager> managerList = managerRepository.findAll();
        assertThat(managerList).hasSize(databaseSizeBeforeCreate + 1);
        Manager testManager = managerList.get(managerList.size() - 1);
        assertThat(testManager.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testManager.getPrenom()).isEqualTo(DEFAULT_PRENOM);
        assertThat(testManager.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testManager.getPassword()).isEqualTo(DEFAULT_PASSWORD);
    }

    @Test
    @Transactional
    public void createManagerWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = managerRepository.findAll().size();

        // Create the Manager with an existing ID
        manager.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restManagerMockMvc.perform(post("/api/managers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(manager)))
            .andExpect(status().isBadRequest());

        // Validate the Manager in the database
        List<Manager> managerList = managerRepository.findAll();
        assertThat(managerList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllManagers() throws Exception {
        // Initialize the database
        managerRepository.saveAndFlush(manager);

        // Get all the managerList
        restManagerMockMvc.perform(get("/api/managers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(manager.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].prenom").value(hasItem(DEFAULT_PRENOM)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].password").value(hasItem(DEFAULT_PASSWORD)));
    }
    
    @Test
    @Transactional
    public void getManager() throws Exception {
        // Initialize the database
        managerRepository.saveAndFlush(manager);

        // Get the manager
        restManagerMockMvc.perform(get("/api/managers/{id}", manager.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(manager.getId().intValue()))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM))
            .andExpect(jsonPath("$.prenom").value(DEFAULT_PRENOM))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.password").value(DEFAULT_PASSWORD));
    }


    @Test
    @Transactional
    public void getManagersByIdFiltering() throws Exception {
        // Initialize the database
        managerRepository.saveAndFlush(manager);

        Long id = manager.getId();

        defaultManagerShouldBeFound("id.equals=" + id);
        defaultManagerShouldNotBeFound("id.notEquals=" + id);

        defaultManagerShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultManagerShouldNotBeFound("id.greaterThan=" + id);

        defaultManagerShouldBeFound("id.lessThanOrEqual=" + id);
        defaultManagerShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllManagersByNomIsEqualToSomething() throws Exception {
        // Initialize the database
        managerRepository.saveAndFlush(manager);

        // Get all the managerList where nom equals to DEFAULT_NOM
        defaultManagerShouldBeFound("nom.equals=" + DEFAULT_NOM);

        // Get all the managerList where nom equals to UPDATED_NOM
        defaultManagerShouldNotBeFound("nom.equals=" + UPDATED_NOM);
    }

    @Test
    @Transactional
    public void getAllManagersByNomIsNotEqualToSomething() throws Exception {
        // Initialize the database
        managerRepository.saveAndFlush(manager);

        // Get all the managerList where nom not equals to DEFAULT_NOM
        defaultManagerShouldNotBeFound("nom.notEquals=" + DEFAULT_NOM);

        // Get all the managerList where nom not equals to UPDATED_NOM
        defaultManagerShouldBeFound("nom.notEquals=" + UPDATED_NOM);
    }

    @Test
    @Transactional
    public void getAllManagersByNomIsInShouldWork() throws Exception {
        // Initialize the database
        managerRepository.saveAndFlush(manager);

        // Get all the managerList where nom in DEFAULT_NOM or UPDATED_NOM
        defaultManagerShouldBeFound("nom.in=" + DEFAULT_NOM + "," + UPDATED_NOM);

        // Get all the managerList where nom equals to UPDATED_NOM
        defaultManagerShouldNotBeFound("nom.in=" + UPDATED_NOM);
    }

    @Test
    @Transactional
    public void getAllManagersByNomIsNullOrNotNull() throws Exception {
        // Initialize the database
        managerRepository.saveAndFlush(manager);

        // Get all the managerList where nom is not null
        defaultManagerShouldBeFound("nom.specified=true");

        // Get all the managerList where nom is null
        defaultManagerShouldNotBeFound("nom.specified=false");
    }
                @Test
    @Transactional
    public void getAllManagersByNomContainsSomething() throws Exception {
        // Initialize the database
        managerRepository.saveAndFlush(manager);

        // Get all the managerList where nom contains DEFAULT_NOM
        defaultManagerShouldBeFound("nom.contains=" + DEFAULT_NOM);

        // Get all the managerList where nom contains UPDATED_NOM
        defaultManagerShouldNotBeFound("nom.contains=" + UPDATED_NOM);
    }

    @Test
    @Transactional
    public void getAllManagersByNomNotContainsSomething() throws Exception {
        // Initialize the database
        managerRepository.saveAndFlush(manager);

        // Get all the managerList where nom does not contain DEFAULT_NOM
        defaultManagerShouldNotBeFound("nom.doesNotContain=" + DEFAULT_NOM);

        // Get all the managerList where nom does not contain UPDATED_NOM
        defaultManagerShouldBeFound("nom.doesNotContain=" + UPDATED_NOM);
    }


    @Test
    @Transactional
    public void getAllManagersByPrenomIsEqualToSomething() throws Exception {
        // Initialize the database
        managerRepository.saveAndFlush(manager);

        // Get all the managerList where prenom equals to DEFAULT_PRENOM
        defaultManagerShouldBeFound("prenom.equals=" + DEFAULT_PRENOM);

        // Get all the managerList where prenom equals to UPDATED_PRENOM
        defaultManagerShouldNotBeFound("prenom.equals=" + UPDATED_PRENOM);
    }

    @Test
    @Transactional
    public void getAllManagersByPrenomIsNotEqualToSomething() throws Exception {
        // Initialize the database
        managerRepository.saveAndFlush(manager);

        // Get all the managerList where prenom not equals to DEFAULT_PRENOM
        defaultManagerShouldNotBeFound("prenom.notEquals=" + DEFAULT_PRENOM);

        // Get all the managerList where prenom not equals to UPDATED_PRENOM
        defaultManagerShouldBeFound("prenom.notEquals=" + UPDATED_PRENOM);
    }

    @Test
    @Transactional
    public void getAllManagersByPrenomIsInShouldWork() throws Exception {
        // Initialize the database
        managerRepository.saveAndFlush(manager);

        // Get all the managerList where prenom in DEFAULT_PRENOM or UPDATED_PRENOM
        defaultManagerShouldBeFound("prenom.in=" + DEFAULT_PRENOM + "," + UPDATED_PRENOM);

        // Get all the managerList where prenom equals to UPDATED_PRENOM
        defaultManagerShouldNotBeFound("prenom.in=" + UPDATED_PRENOM);
    }

    @Test
    @Transactional
    public void getAllManagersByPrenomIsNullOrNotNull() throws Exception {
        // Initialize the database
        managerRepository.saveAndFlush(manager);

        // Get all the managerList where prenom is not null
        defaultManagerShouldBeFound("prenom.specified=true");

        // Get all the managerList where prenom is null
        defaultManagerShouldNotBeFound("prenom.specified=false");
    }
                @Test
    @Transactional
    public void getAllManagersByPrenomContainsSomething() throws Exception {
        // Initialize the database
        managerRepository.saveAndFlush(manager);

        // Get all the managerList where prenom contains DEFAULT_PRENOM
        defaultManagerShouldBeFound("prenom.contains=" + DEFAULT_PRENOM);

        // Get all the managerList where prenom contains UPDATED_PRENOM
        defaultManagerShouldNotBeFound("prenom.contains=" + UPDATED_PRENOM);
    }

    @Test
    @Transactional
    public void getAllManagersByPrenomNotContainsSomething() throws Exception {
        // Initialize the database
        managerRepository.saveAndFlush(manager);

        // Get all the managerList where prenom does not contain DEFAULT_PRENOM
        defaultManagerShouldNotBeFound("prenom.doesNotContain=" + DEFAULT_PRENOM);

        // Get all the managerList where prenom does not contain UPDATED_PRENOM
        defaultManagerShouldBeFound("prenom.doesNotContain=" + UPDATED_PRENOM);
    }


    @Test
    @Transactional
    public void getAllManagersByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        managerRepository.saveAndFlush(manager);

        // Get all the managerList where email equals to DEFAULT_EMAIL
        defaultManagerShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the managerList where email equals to UPDATED_EMAIL
        defaultManagerShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllManagersByEmailIsNotEqualToSomething() throws Exception {
        // Initialize the database
        managerRepository.saveAndFlush(manager);

        // Get all the managerList where email not equals to DEFAULT_EMAIL
        defaultManagerShouldNotBeFound("email.notEquals=" + DEFAULT_EMAIL);

        // Get all the managerList where email not equals to UPDATED_EMAIL
        defaultManagerShouldBeFound("email.notEquals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllManagersByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        managerRepository.saveAndFlush(manager);

        // Get all the managerList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultManagerShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the managerList where email equals to UPDATED_EMAIL
        defaultManagerShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllManagersByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        managerRepository.saveAndFlush(manager);

        // Get all the managerList where email is not null
        defaultManagerShouldBeFound("email.specified=true");

        // Get all the managerList where email is null
        defaultManagerShouldNotBeFound("email.specified=false");
    }
                @Test
    @Transactional
    public void getAllManagersByEmailContainsSomething() throws Exception {
        // Initialize the database
        managerRepository.saveAndFlush(manager);

        // Get all the managerList where email contains DEFAULT_EMAIL
        defaultManagerShouldBeFound("email.contains=" + DEFAULT_EMAIL);

        // Get all the managerList where email contains UPDATED_EMAIL
        defaultManagerShouldNotBeFound("email.contains=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllManagersByEmailNotContainsSomething() throws Exception {
        // Initialize the database
        managerRepository.saveAndFlush(manager);

        // Get all the managerList where email does not contain DEFAULT_EMAIL
        defaultManagerShouldNotBeFound("email.doesNotContain=" + DEFAULT_EMAIL);

        // Get all the managerList where email does not contain UPDATED_EMAIL
        defaultManagerShouldBeFound("email.doesNotContain=" + UPDATED_EMAIL);
    }


    @Test
    @Transactional
    public void getAllManagersByPasswordIsEqualToSomething() throws Exception {
        // Initialize the database
        managerRepository.saveAndFlush(manager);

        // Get all the managerList where password equals to DEFAULT_PASSWORD
        defaultManagerShouldBeFound("password.equals=" + DEFAULT_PASSWORD);

        // Get all the managerList where password equals to UPDATED_PASSWORD
        defaultManagerShouldNotBeFound("password.equals=" + UPDATED_PASSWORD);
    }

    @Test
    @Transactional
    public void getAllManagersByPasswordIsNotEqualToSomething() throws Exception {
        // Initialize the database
        managerRepository.saveAndFlush(manager);

        // Get all the managerList where password not equals to DEFAULT_PASSWORD
        defaultManagerShouldNotBeFound("password.notEquals=" + DEFAULT_PASSWORD);

        // Get all the managerList where password not equals to UPDATED_PASSWORD
        defaultManagerShouldBeFound("password.notEquals=" + UPDATED_PASSWORD);
    }

    @Test
    @Transactional
    public void getAllManagersByPasswordIsInShouldWork() throws Exception {
        // Initialize the database
        managerRepository.saveAndFlush(manager);

        // Get all the managerList where password in DEFAULT_PASSWORD or UPDATED_PASSWORD
        defaultManagerShouldBeFound("password.in=" + DEFAULT_PASSWORD + "," + UPDATED_PASSWORD);

        // Get all the managerList where password equals to UPDATED_PASSWORD
        defaultManagerShouldNotBeFound("password.in=" + UPDATED_PASSWORD);
    }

    @Test
    @Transactional
    public void getAllManagersByPasswordIsNullOrNotNull() throws Exception {
        // Initialize the database
        managerRepository.saveAndFlush(manager);

        // Get all the managerList where password is not null
        defaultManagerShouldBeFound("password.specified=true");

        // Get all the managerList where password is null
        defaultManagerShouldNotBeFound("password.specified=false");
    }
                @Test
    @Transactional
    public void getAllManagersByPasswordContainsSomething() throws Exception {
        // Initialize the database
        managerRepository.saveAndFlush(manager);

        // Get all the managerList where password contains DEFAULT_PASSWORD
        defaultManagerShouldBeFound("password.contains=" + DEFAULT_PASSWORD);

        // Get all the managerList where password contains UPDATED_PASSWORD
        defaultManagerShouldNotBeFound("password.contains=" + UPDATED_PASSWORD);
    }

    @Test
    @Transactional
    public void getAllManagersByPasswordNotContainsSomething() throws Exception {
        // Initialize the database
        managerRepository.saveAndFlush(manager);

        // Get all the managerList where password does not contain DEFAULT_PASSWORD
        defaultManagerShouldNotBeFound("password.doesNotContain=" + DEFAULT_PASSWORD);

        // Get all the managerList where password does not contain UPDATED_PASSWORD
        defaultManagerShouldBeFound("password.doesNotContain=" + UPDATED_PASSWORD);
    }


    @Test
    @Transactional
    public void getAllManagersByUserIsEqualToSomething() throws Exception {
        // Initialize the database
        managerRepository.saveAndFlush(manager);
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        manager.setUser(user);
        managerRepository.saveAndFlush(manager);
        Long userId = user.getId();

        // Get all the managerList where user equals to userId
        defaultManagerShouldBeFound("userId.equals=" + userId);

        // Get all the managerList where user equals to userId + 1
        defaultManagerShouldNotBeFound("userId.equals=" + (userId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultManagerShouldBeFound(String filter) throws Exception {
        restManagerMockMvc.perform(get("/api/managers?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(manager.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].prenom").value(hasItem(DEFAULT_PRENOM)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].password").value(hasItem(DEFAULT_PASSWORD)));

        // Check, that the count call also returns 1
        restManagerMockMvc.perform(get("/api/managers/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultManagerShouldNotBeFound(String filter) throws Exception {
        restManagerMockMvc.perform(get("/api/managers?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restManagerMockMvc.perform(get("/api/managers/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingManager() throws Exception {
        // Get the manager
        restManagerMockMvc.perform(get("/api/managers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateManager() throws Exception {
        // Initialize the database
        managerService.save(manager);

        int databaseSizeBeforeUpdate = managerRepository.findAll().size();

        // Update the manager
        Manager updatedManager = managerRepository.findById(manager.getId()).get();
        // Disconnect from session so that the updates on updatedManager are not directly saved in db
        em.detach(updatedManager);
        updatedManager
            .nom(UPDATED_NOM)
            .prenom(UPDATED_PRENOM)
            .email(UPDATED_EMAIL)
            .password(UPDATED_PASSWORD);

        restManagerMockMvc.perform(put("/api/managers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedManager)))
            .andExpect(status().isOk());

        // Validate the Manager in the database
        List<Manager> managerList = managerRepository.findAll();
        assertThat(managerList).hasSize(databaseSizeBeforeUpdate);
        Manager testManager = managerList.get(managerList.size() - 1);
        assertThat(testManager.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testManager.getPrenom()).isEqualTo(UPDATED_PRENOM);
        assertThat(testManager.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testManager.getPassword()).isEqualTo(UPDATED_PASSWORD);
    }

    @Test
    @Transactional
    public void updateNonExistingManager() throws Exception {
        int databaseSizeBeforeUpdate = managerRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restManagerMockMvc.perform(put("/api/managers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(manager)))
            .andExpect(status().isBadRequest());

        // Validate the Manager in the database
        List<Manager> managerList = managerRepository.findAll();
        assertThat(managerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteManager() throws Exception {
        // Initialize the database
        managerService.save(manager);

        int databaseSizeBeforeDelete = managerRepository.findAll().size();

        // Delete the manager
        restManagerMockMvc.perform(delete("/api/managers/{id}", manager.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Manager> managerList = managerRepository.findAll();
        assertThat(managerList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
