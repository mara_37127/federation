package sn.federation.web.rest;

import sn.federation.FederationApp;
import sn.federation.domain.Joueur;
import sn.federation.domain.Equipe;
import sn.federation.repository.JoueurRepository;
import sn.federation.service.JoueurService;
import sn.federation.service.dto.JoueurCriteria;
import sn.federation.service.JoueurQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link JoueurResource} REST controller.
 */
@SpringBootTest(classes = FederationApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class JoueurResourceIT {

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    private static final String DEFAULT_PRENOM = "AAAAAAAAAA";
    private static final String UPDATED_PRENOM = "BBBBBBBBBB";

    private static final Integer DEFAULT_AGE = 1;
    private static final Integer UPDATED_AGE = 2;
    private static final Integer SMALLER_AGE = 1 - 1;

    private static final String DEFAULT_POSTE = "AAAAAAAAAA";
    private static final String UPDATED_POSTE = "BBBBBBBBBB";

    @Autowired
    private JoueurRepository joueurRepository;

    @Autowired
    private JoueurService joueurService;

    @Autowired
    private JoueurQueryService joueurQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restJoueurMockMvc;

    private Joueur joueur;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Joueur createEntity(EntityManager em) {
        Joueur joueur = new Joueur()
            .nom(DEFAULT_NOM)
            .prenom(DEFAULT_PRENOM)
            .age(DEFAULT_AGE)
            .poste(DEFAULT_POSTE);
        return joueur;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Joueur createUpdatedEntity(EntityManager em) {
        Joueur joueur = new Joueur()
            .nom(UPDATED_NOM)
            .prenom(UPDATED_PRENOM)
            .age(UPDATED_AGE)
            .poste(UPDATED_POSTE);
        return joueur;
    }

    @BeforeEach
    public void initTest() {
        joueur = createEntity(em);
    }

    @Test
    @Transactional
    public void createJoueur() throws Exception {
        int databaseSizeBeforeCreate = joueurRepository.findAll().size();
        // Create the Joueur
        restJoueurMockMvc.perform(post("/api/joueurs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(joueur)))
            .andExpect(status().isCreated());

        // Validate the Joueur in the database
        List<Joueur> joueurList = joueurRepository.findAll();
        assertThat(joueurList).hasSize(databaseSizeBeforeCreate + 1);
        Joueur testJoueur = joueurList.get(joueurList.size() - 1);
        assertThat(testJoueur.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testJoueur.getPrenom()).isEqualTo(DEFAULT_PRENOM);
        assertThat(testJoueur.getAge()).isEqualTo(DEFAULT_AGE);
        assertThat(testJoueur.getPoste()).isEqualTo(DEFAULT_POSTE);
    }

    @Test
    @Transactional
    public void createJoueurWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = joueurRepository.findAll().size();

        // Create the Joueur with an existing ID
        joueur.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restJoueurMockMvc.perform(post("/api/joueurs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(joueur)))
            .andExpect(status().isBadRequest());

        // Validate the Joueur in the database
        List<Joueur> joueurList = joueurRepository.findAll();
        assertThat(joueurList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllJoueurs() throws Exception {
        // Initialize the database
        joueurRepository.saveAndFlush(joueur);

        // Get all the joueurList
        restJoueurMockMvc.perform(get("/api/joueurs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(joueur.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].prenom").value(hasItem(DEFAULT_PRENOM)))
            .andExpect(jsonPath("$.[*].age").value(hasItem(DEFAULT_AGE)))
            .andExpect(jsonPath("$.[*].poste").value(hasItem(DEFAULT_POSTE)));
    }
    
    @Test
    @Transactional
    public void getJoueur() throws Exception {
        // Initialize the database
        joueurRepository.saveAndFlush(joueur);

        // Get the joueur
        restJoueurMockMvc.perform(get("/api/joueurs/{id}", joueur.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(joueur.getId().intValue()))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM))
            .andExpect(jsonPath("$.prenom").value(DEFAULT_PRENOM))
            .andExpect(jsonPath("$.age").value(DEFAULT_AGE))
            .andExpect(jsonPath("$.poste").value(DEFAULT_POSTE));
    }


    @Test
    @Transactional
    public void getJoueursByIdFiltering() throws Exception {
        // Initialize the database
        joueurRepository.saveAndFlush(joueur);

        Long id = joueur.getId();

        defaultJoueurShouldBeFound("id.equals=" + id);
        defaultJoueurShouldNotBeFound("id.notEquals=" + id);

        defaultJoueurShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultJoueurShouldNotBeFound("id.greaterThan=" + id);

        defaultJoueurShouldBeFound("id.lessThanOrEqual=" + id);
        defaultJoueurShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllJoueursByNomIsEqualToSomething() throws Exception {
        // Initialize the database
        joueurRepository.saveAndFlush(joueur);

        // Get all the joueurList where nom equals to DEFAULT_NOM
        defaultJoueurShouldBeFound("nom.equals=" + DEFAULT_NOM);

        // Get all the joueurList where nom equals to UPDATED_NOM
        defaultJoueurShouldNotBeFound("nom.equals=" + UPDATED_NOM);
    }

    @Test
    @Transactional
    public void getAllJoueursByNomIsNotEqualToSomething() throws Exception {
        // Initialize the database
        joueurRepository.saveAndFlush(joueur);

        // Get all the joueurList where nom not equals to DEFAULT_NOM
        defaultJoueurShouldNotBeFound("nom.notEquals=" + DEFAULT_NOM);

        // Get all the joueurList where nom not equals to UPDATED_NOM
        defaultJoueurShouldBeFound("nom.notEquals=" + UPDATED_NOM);
    }

    @Test
    @Transactional
    public void getAllJoueursByNomIsInShouldWork() throws Exception {
        // Initialize the database
        joueurRepository.saveAndFlush(joueur);

        // Get all the joueurList where nom in DEFAULT_NOM or UPDATED_NOM
        defaultJoueurShouldBeFound("nom.in=" + DEFAULT_NOM + "," + UPDATED_NOM);

        // Get all the joueurList where nom equals to UPDATED_NOM
        defaultJoueurShouldNotBeFound("nom.in=" + UPDATED_NOM);
    }

    @Test
    @Transactional
    public void getAllJoueursByNomIsNullOrNotNull() throws Exception {
        // Initialize the database
        joueurRepository.saveAndFlush(joueur);

        // Get all the joueurList where nom is not null
        defaultJoueurShouldBeFound("nom.specified=true");

        // Get all the joueurList where nom is null
        defaultJoueurShouldNotBeFound("nom.specified=false");
    }
                @Test
    @Transactional
    public void getAllJoueursByNomContainsSomething() throws Exception {
        // Initialize the database
        joueurRepository.saveAndFlush(joueur);

        // Get all the joueurList where nom contains DEFAULT_NOM
        defaultJoueurShouldBeFound("nom.contains=" + DEFAULT_NOM);

        // Get all the joueurList where nom contains UPDATED_NOM
        defaultJoueurShouldNotBeFound("nom.contains=" + UPDATED_NOM);
    }

    @Test
    @Transactional
    public void getAllJoueursByNomNotContainsSomething() throws Exception {
        // Initialize the database
        joueurRepository.saveAndFlush(joueur);

        // Get all the joueurList where nom does not contain DEFAULT_NOM
        defaultJoueurShouldNotBeFound("nom.doesNotContain=" + DEFAULT_NOM);

        // Get all the joueurList where nom does not contain UPDATED_NOM
        defaultJoueurShouldBeFound("nom.doesNotContain=" + UPDATED_NOM);
    }


    @Test
    @Transactional
    public void getAllJoueursByPrenomIsEqualToSomething() throws Exception {
        // Initialize the database
        joueurRepository.saveAndFlush(joueur);

        // Get all the joueurList where prenom equals to DEFAULT_PRENOM
        defaultJoueurShouldBeFound("prenom.equals=" + DEFAULT_PRENOM);

        // Get all the joueurList where prenom equals to UPDATED_PRENOM
        defaultJoueurShouldNotBeFound("prenom.equals=" + UPDATED_PRENOM);
    }

    @Test
    @Transactional
    public void getAllJoueursByPrenomIsNotEqualToSomething() throws Exception {
        // Initialize the database
        joueurRepository.saveAndFlush(joueur);

        // Get all the joueurList where prenom not equals to DEFAULT_PRENOM
        defaultJoueurShouldNotBeFound("prenom.notEquals=" + DEFAULT_PRENOM);

        // Get all the joueurList where prenom not equals to UPDATED_PRENOM
        defaultJoueurShouldBeFound("prenom.notEquals=" + UPDATED_PRENOM);
    }

    @Test
    @Transactional
    public void getAllJoueursByPrenomIsInShouldWork() throws Exception {
        // Initialize the database
        joueurRepository.saveAndFlush(joueur);

        // Get all the joueurList where prenom in DEFAULT_PRENOM or UPDATED_PRENOM
        defaultJoueurShouldBeFound("prenom.in=" + DEFAULT_PRENOM + "," + UPDATED_PRENOM);

        // Get all the joueurList where prenom equals to UPDATED_PRENOM
        defaultJoueurShouldNotBeFound("prenom.in=" + UPDATED_PRENOM);
    }

    @Test
    @Transactional
    public void getAllJoueursByPrenomIsNullOrNotNull() throws Exception {
        // Initialize the database
        joueurRepository.saveAndFlush(joueur);

        // Get all the joueurList where prenom is not null
        defaultJoueurShouldBeFound("prenom.specified=true");

        // Get all the joueurList where prenom is null
        defaultJoueurShouldNotBeFound("prenom.specified=false");
    }
                @Test
    @Transactional
    public void getAllJoueursByPrenomContainsSomething() throws Exception {
        // Initialize the database
        joueurRepository.saveAndFlush(joueur);

        // Get all the joueurList where prenom contains DEFAULT_PRENOM
        defaultJoueurShouldBeFound("prenom.contains=" + DEFAULT_PRENOM);

        // Get all the joueurList where prenom contains UPDATED_PRENOM
        defaultJoueurShouldNotBeFound("prenom.contains=" + UPDATED_PRENOM);
    }

    @Test
    @Transactional
    public void getAllJoueursByPrenomNotContainsSomething() throws Exception {
        // Initialize the database
        joueurRepository.saveAndFlush(joueur);

        // Get all the joueurList where prenom does not contain DEFAULT_PRENOM
        defaultJoueurShouldNotBeFound("prenom.doesNotContain=" + DEFAULT_PRENOM);

        // Get all the joueurList where prenom does not contain UPDATED_PRENOM
        defaultJoueurShouldBeFound("prenom.doesNotContain=" + UPDATED_PRENOM);
    }


    @Test
    @Transactional
    public void getAllJoueursByAgeIsEqualToSomething() throws Exception {
        // Initialize the database
        joueurRepository.saveAndFlush(joueur);

        // Get all the joueurList where age equals to DEFAULT_AGE
        defaultJoueurShouldBeFound("age.equals=" + DEFAULT_AGE);

        // Get all the joueurList where age equals to UPDATED_AGE
        defaultJoueurShouldNotBeFound("age.equals=" + UPDATED_AGE);
    }

    @Test
    @Transactional
    public void getAllJoueursByAgeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        joueurRepository.saveAndFlush(joueur);

        // Get all the joueurList where age not equals to DEFAULT_AGE
        defaultJoueurShouldNotBeFound("age.notEquals=" + DEFAULT_AGE);

        // Get all the joueurList where age not equals to UPDATED_AGE
        defaultJoueurShouldBeFound("age.notEquals=" + UPDATED_AGE);
    }

    @Test
    @Transactional
    public void getAllJoueursByAgeIsInShouldWork() throws Exception {
        // Initialize the database
        joueurRepository.saveAndFlush(joueur);

        // Get all the joueurList where age in DEFAULT_AGE or UPDATED_AGE
        defaultJoueurShouldBeFound("age.in=" + DEFAULT_AGE + "," + UPDATED_AGE);

        // Get all the joueurList where age equals to UPDATED_AGE
        defaultJoueurShouldNotBeFound("age.in=" + UPDATED_AGE);
    }

    @Test
    @Transactional
    public void getAllJoueursByAgeIsNullOrNotNull() throws Exception {
        // Initialize the database
        joueurRepository.saveAndFlush(joueur);

        // Get all the joueurList where age is not null
        defaultJoueurShouldBeFound("age.specified=true");

        // Get all the joueurList where age is null
        defaultJoueurShouldNotBeFound("age.specified=false");
    }

    @Test
    @Transactional
    public void getAllJoueursByAgeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        joueurRepository.saveAndFlush(joueur);

        // Get all the joueurList where age is greater than or equal to DEFAULT_AGE
        defaultJoueurShouldBeFound("age.greaterThanOrEqual=" + DEFAULT_AGE);

        // Get all the joueurList where age is greater than or equal to UPDATED_AGE
        defaultJoueurShouldNotBeFound("age.greaterThanOrEqual=" + UPDATED_AGE);
    }

    @Test
    @Transactional
    public void getAllJoueursByAgeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        joueurRepository.saveAndFlush(joueur);

        // Get all the joueurList where age is less than or equal to DEFAULT_AGE
        defaultJoueurShouldBeFound("age.lessThanOrEqual=" + DEFAULT_AGE);

        // Get all the joueurList where age is less than or equal to SMALLER_AGE
        defaultJoueurShouldNotBeFound("age.lessThanOrEqual=" + SMALLER_AGE);
    }

    @Test
    @Transactional
    public void getAllJoueursByAgeIsLessThanSomething() throws Exception {
        // Initialize the database
        joueurRepository.saveAndFlush(joueur);

        // Get all the joueurList where age is less than DEFAULT_AGE
        defaultJoueurShouldNotBeFound("age.lessThan=" + DEFAULT_AGE);

        // Get all the joueurList where age is less than UPDATED_AGE
        defaultJoueurShouldBeFound("age.lessThan=" + UPDATED_AGE);
    }

    @Test
    @Transactional
    public void getAllJoueursByAgeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        joueurRepository.saveAndFlush(joueur);

        // Get all the joueurList where age is greater than DEFAULT_AGE
        defaultJoueurShouldNotBeFound("age.greaterThan=" + DEFAULT_AGE);

        // Get all the joueurList where age is greater than SMALLER_AGE
        defaultJoueurShouldBeFound("age.greaterThan=" + SMALLER_AGE);
    }


    @Test
    @Transactional
    public void getAllJoueursByPosteIsEqualToSomething() throws Exception {
        // Initialize the database
        joueurRepository.saveAndFlush(joueur);

        // Get all the joueurList where poste equals to DEFAULT_POSTE
        defaultJoueurShouldBeFound("poste.equals=" + DEFAULT_POSTE);

        // Get all the joueurList where poste equals to UPDATED_POSTE
        defaultJoueurShouldNotBeFound("poste.equals=" + UPDATED_POSTE);
    }

    @Test
    @Transactional
    public void getAllJoueursByPosteIsNotEqualToSomething() throws Exception {
        // Initialize the database
        joueurRepository.saveAndFlush(joueur);

        // Get all the joueurList where poste not equals to DEFAULT_POSTE
        defaultJoueurShouldNotBeFound("poste.notEquals=" + DEFAULT_POSTE);

        // Get all the joueurList where poste not equals to UPDATED_POSTE
        defaultJoueurShouldBeFound("poste.notEquals=" + UPDATED_POSTE);
    }

    @Test
    @Transactional
    public void getAllJoueursByPosteIsInShouldWork() throws Exception {
        // Initialize the database
        joueurRepository.saveAndFlush(joueur);

        // Get all the joueurList where poste in DEFAULT_POSTE or UPDATED_POSTE
        defaultJoueurShouldBeFound("poste.in=" + DEFAULT_POSTE + "," + UPDATED_POSTE);

        // Get all the joueurList where poste equals to UPDATED_POSTE
        defaultJoueurShouldNotBeFound("poste.in=" + UPDATED_POSTE);
    }

    @Test
    @Transactional
    public void getAllJoueursByPosteIsNullOrNotNull() throws Exception {
        // Initialize the database
        joueurRepository.saveAndFlush(joueur);

        // Get all the joueurList where poste is not null
        defaultJoueurShouldBeFound("poste.specified=true");

        // Get all the joueurList where poste is null
        defaultJoueurShouldNotBeFound("poste.specified=false");
    }
                @Test
    @Transactional
    public void getAllJoueursByPosteContainsSomething() throws Exception {
        // Initialize the database
        joueurRepository.saveAndFlush(joueur);

        // Get all the joueurList where poste contains DEFAULT_POSTE
        defaultJoueurShouldBeFound("poste.contains=" + DEFAULT_POSTE);

        // Get all the joueurList where poste contains UPDATED_POSTE
        defaultJoueurShouldNotBeFound("poste.contains=" + UPDATED_POSTE);
    }

    @Test
    @Transactional
    public void getAllJoueursByPosteNotContainsSomething() throws Exception {
        // Initialize the database
        joueurRepository.saveAndFlush(joueur);

        // Get all the joueurList where poste does not contain DEFAULT_POSTE
        defaultJoueurShouldNotBeFound("poste.doesNotContain=" + DEFAULT_POSTE);

        // Get all the joueurList where poste does not contain UPDATED_POSTE
        defaultJoueurShouldBeFound("poste.doesNotContain=" + UPDATED_POSTE);
    }


    @Test
    @Transactional
    public void getAllJoueursByEquipeIsEqualToSomething() throws Exception {
        // Initialize the database
        joueurRepository.saveAndFlush(joueur);
        Equipe equipe = EquipeResourceIT.createEntity(em);
        em.persist(equipe);
        em.flush();
        joueur.setEquipe(equipe);
        joueurRepository.saveAndFlush(joueur);
        Long equipeId = equipe.getId();

        // Get all the joueurList where equipe equals to equipeId
        defaultJoueurShouldBeFound("equipeId.equals=" + equipeId);

        // Get all the joueurList where equipe equals to equipeId + 1
        defaultJoueurShouldNotBeFound("equipeId.equals=" + (equipeId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultJoueurShouldBeFound(String filter) throws Exception {
        restJoueurMockMvc.perform(get("/api/joueurs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(joueur.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].prenom").value(hasItem(DEFAULT_PRENOM)))
            .andExpect(jsonPath("$.[*].age").value(hasItem(DEFAULT_AGE)))
            .andExpect(jsonPath("$.[*].poste").value(hasItem(DEFAULT_POSTE)));

        // Check, that the count call also returns 1
        restJoueurMockMvc.perform(get("/api/joueurs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultJoueurShouldNotBeFound(String filter) throws Exception {
        restJoueurMockMvc.perform(get("/api/joueurs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restJoueurMockMvc.perform(get("/api/joueurs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingJoueur() throws Exception {
        // Get the joueur
        restJoueurMockMvc.perform(get("/api/joueurs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateJoueur() throws Exception {
        // Initialize the database
        joueurService.save(joueur);

        int databaseSizeBeforeUpdate = joueurRepository.findAll().size();

        // Update the joueur
        Joueur updatedJoueur = joueurRepository.findById(joueur.getId()).get();
        // Disconnect from session so that the updates on updatedJoueur are not directly saved in db
        em.detach(updatedJoueur);
        updatedJoueur
            .nom(UPDATED_NOM)
            .prenom(UPDATED_PRENOM)
            .age(UPDATED_AGE)
            .poste(UPDATED_POSTE);

        restJoueurMockMvc.perform(put("/api/joueurs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedJoueur)))
            .andExpect(status().isOk());

        // Validate the Joueur in the database
        List<Joueur> joueurList = joueurRepository.findAll();
        assertThat(joueurList).hasSize(databaseSizeBeforeUpdate);
        Joueur testJoueur = joueurList.get(joueurList.size() - 1);
        assertThat(testJoueur.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testJoueur.getPrenom()).isEqualTo(UPDATED_PRENOM);
        assertThat(testJoueur.getAge()).isEqualTo(UPDATED_AGE);
        assertThat(testJoueur.getPoste()).isEqualTo(UPDATED_POSTE);
    }

    @Test
    @Transactional
    public void updateNonExistingJoueur() throws Exception {
        int databaseSizeBeforeUpdate = joueurRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restJoueurMockMvc.perform(put("/api/joueurs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(joueur)))
            .andExpect(status().isBadRequest());

        // Validate the Joueur in the database
        List<Joueur> joueurList = joueurRepository.findAll();
        assertThat(joueurList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteJoueur() throws Exception {
        // Initialize the database
        joueurService.save(joueur);

        int databaseSizeBeforeDelete = joueurRepository.findAll().size();

        // Delete the joueur
        restJoueurMockMvc.perform(delete("/api/joueurs/{id}", joueur.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Joueur> joueurList = joueurRepository.findAll();
        assertThat(joueurList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
