package sn.federation.service.impl;

import sn.federation.domain.Authority;
import sn.federation.domain.User;
import sn.federation.repository.AuthorityRepository;
import sn.federation.security.AuthoritiesConstants;
import sn.federation.service.ManagerService;
import sn.federation.domain.Manager;
import sn.federation.repository.ManagerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.federation.service.UserService;
import sn.federation.service.dto.UserDTO;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * Service Implementation for managing {@link Manager}.
 */
@Service
@Transactional
public class ManagerServiceImpl implements ManagerService {

    private final Logger log = LoggerFactory.getLogger(ManagerServiceImpl.class);

    private final ManagerRepository managerRepository;
    private final UserService userService;
    private final AuthorityRepository authorityRepository;
    public ManagerServiceImpl(ManagerRepository managerRepository, AuthorityRepository authorityRepository, UserService userService) {

        this.managerRepository = managerRepository;
        this.authorityRepository = authorityRepository;
        this.userService = userService;
    }

    /**
     * Save a manager.
     *
     * @param manager the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Manager save(Manager manager) {
        log.debug("Request to save Manager : {}", manager);

        // création du DTO User
        UserDTO userDTO = new UserDTO();
        userDTO.setFirstName(manager.getPrenom());
        userDTO.setLastName(manager.getNom());
        userDTO.setEmail(manager.getEmail());
        userDTO.setLogin(manager.getPrenom()+"."+manager.getNom());

        // créeation d'un User à partir de userDTO
        User user = userService.createUser(userDTO);

        // ajout du role User
        Set<Authority> authoritySet = new HashSet<>();
        authorityRepository.findById(AuthoritiesConstants.USER).ifPresent(authoritySet::add);
        user.setAuthorities(authoritySet);

        // ajout du User nouvellement créé à Manager
        manager.setUser(user);

        // création du Manager
        managerRepository.save(manager);

        return manager;

    }

    /**
     * Get all the managers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Manager> findAll(Pageable pageable) {
        log.debug("Request to get all Managers");
        return managerRepository.findAll(pageable);
    }


    /**
     * Get one manager by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Manager> findOne(Long id) {
        log.debug("Request to get Manager : {}", id);
        return managerRepository.findById(id);
    }

    /**
     * Delete the manager by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Manager : {}", id);
        managerRepository.deleteById(id);
    }
}
