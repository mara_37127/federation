package sn.federation.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link sn.federation.domain.Joueur} entity. This class is used
 * in {@link sn.federation.web.rest.JoueurResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /joueurs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class JoueurCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter nom;

    private StringFilter prenom;

    private IntegerFilter age;

    private StringFilter poste;

    private LongFilter equipeId;

    public JoueurCriteria() {
    }

    public JoueurCriteria(JoueurCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.nom = other.nom == null ? null : other.nom.copy();
        this.prenom = other.prenom == null ? null : other.prenom.copy();
        this.age = other.age == null ? null : other.age.copy();
        this.poste = other.poste == null ? null : other.poste.copy();
        this.equipeId = other.equipeId == null ? null : other.equipeId.copy();
    }

    @Override
    public JoueurCriteria copy() {
        return new JoueurCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getNom() {
        return nom;
    }

    public void setNom(StringFilter nom) {
        this.nom = nom;
    }

    public StringFilter getPrenom() {
        return prenom;
    }

    public void setPrenom(StringFilter prenom) {
        this.prenom = prenom;
    }

    public IntegerFilter getAge() {
        return age;
    }

    public void setAge(IntegerFilter age) {
        this.age = age;
    }

    public StringFilter getPoste() {
        return poste;
    }

    public void setPoste(StringFilter poste) {
        this.poste = poste;
    }

    public LongFilter getEquipeId() {
        return equipeId;
    }

    public void setEquipeId(LongFilter equipeId) {
        this.equipeId = equipeId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final JoueurCriteria that = (JoueurCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(nom, that.nom) &&
            Objects.equals(prenom, that.prenom) &&
            Objects.equals(age, that.age) &&
            Objects.equals(poste, that.poste) &&
            Objects.equals(equipeId, that.equipeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        nom,
        prenom,
        age,
        poste,
        equipeId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "JoueurCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (nom != null ? "nom=" + nom + ", " : "") +
                (prenom != null ? "prenom=" + prenom + ", " : "") +
                (age != null ? "age=" + age + ", " : "") +
                (poste != null ? "poste=" + poste + ", " : "") +
                (equipeId != null ? "equipeId=" + equipeId + ", " : "") +
            "}";
    }

}
