package sn.federation.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link sn.federation.domain.Equipe} entity. This class is used
 * in {@link sn.federation.web.rest.EquipeResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /equipes?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class EquipeCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter nom;

    private LocalDateFilter dateCreation;

    private LongFilter coachId;

    private LongFilter joueurId;

    public EquipeCriteria() {
    }

    public EquipeCriteria(EquipeCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.nom = other.nom == null ? null : other.nom.copy();
        this.dateCreation = other.dateCreation == null ? null : other.dateCreation.copy();
        this.coachId = other.coachId == null ? null : other.coachId.copy();
        this.joueurId = other.joueurId == null ? null : other.joueurId.copy();
    }

    @Override
    public EquipeCriteria copy() {
        return new EquipeCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getNom() {
        return nom;
    }

    public void setNom(StringFilter nom) {
        this.nom = nom;
    }

    public LocalDateFilter getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(LocalDateFilter dateCreation) {
        this.dateCreation = dateCreation;
    }

    public LongFilter getCoachId() {
        return coachId;
    }

    public void setCoachId(LongFilter coachId) {
        this.coachId = coachId;
    }

    public LongFilter getJoueurId() {
        return joueurId;
    }

    public void setJoueurId(LongFilter joueurId) {
        this.joueurId = joueurId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final EquipeCriteria that = (EquipeCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(nom, that.nom) &&
            Objects.equals(dateCreation, that.dateCreation) &&
            Objects.equals(coachId, that.coachId) &&
            Objects.equals(joueurId, that.joueurId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        nom,
        dateCreation,
        coachId,
        joueurId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "EquipeCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (nom != null ? "nom=" + nom + ", " : "") +
                (dateCreation != null ? "dateCreation=" + dateCreation + ", " : "") +
                (coachId != null ? "coachId=" + coachId + ", " : "") +
                (joueurId != null ? "joueurId=" + joueurId + ", " : "") +
            "}";
    }

}
