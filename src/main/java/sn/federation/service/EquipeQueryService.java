package sn.federation.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import sn.federation.domain.Equipe;
import sn.federation.domain.*; // for static metamodels
import sn.federation.repository.EquipeRepository;
import sn.federation.service.dto.EquipeCriteria;

/**
 * Service for executing complex queries for {@link Equipe} entities in the database.
 * The main input is a {@link EquipeCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Equipe} or a {@link Page} of {@link Equipe} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EquipeQueryService extends QueryService<Equipe> {

    private final Logger log = LoggerFactory.getLogger(EquipeQueryService.class);

    private final EquipeRepository equipeRepository;

    public EquipeQueryService(EquipeRepository equipeRepository) {
        this.equipeRepository = equipeRepository;
    }

    /**
     * Return a {@link List} of {@link Equipe} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Equipe> findByCriteria(EquipeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Equipe> specification = createSpecification(criteria);
        return equipeRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Equipe} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Equipe> findByCriteria(EquipeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Equipe> specification = createSpecification(criteria);
        return equipeRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(EquipeCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Equipe> specification = createSpecification(criteria);
        return equipeRepository.count(specification);
    }

    /**
     * Function to convert {@link EquipeCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Equipe> createSpecification(EquipeCriteria criteria) {
        Specification<Equipe> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Equipe_.id));
            }
            if (criteria.getNom() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNom(), Equipe_.nom));
            }
            if (criteria.getDateCreation() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateCreation(), Equipe_.dateCreation));
            }
            if (criteria.getCoachId() != null) {
                specification = specification.and(buildSpecification(criteria.getCoachId(),
                    root -> root.join(Equipe_.coach, JoinType.LEFT).get(Coach_.id)));
            }
            if (criteria.getJoueurId() != null) {
                specification = specification.and(buildSpecification(criteria.getJoueurId(),
                    root -> root.join(Equipe_.joueurs, JoinType.LEFT).get(Joueur_.id)));
            }
        }
        return specification;
    }
}
