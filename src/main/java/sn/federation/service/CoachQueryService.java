package sn.federation.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import sn.federation.domain.Coach;
import sn.federation.domain.*; // for static metamodels
import sn.federation.repository.CoachRepository;
import sn.federation.service.dto.CoachCriteria;

/**
 * Service for executing complex queries for {@link Coach} entities in the database.
 * The main input is a {@link CoachCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Coach} or a {@link Page} of {@link Coach} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CoachQueryService extends QueryService<Coach> {

    private final Logger log = LoggerFactory.getLogger(CoachQueryService.class);

    private final CoachRepository coachRepository;

    public CoachQueryService(CoachRepository coachRepository) {
        this.coachRepository = coachRepository;
    }

    /**
     * Return a {@link List} of {@link Coach} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Coach> findByCriteria(CoachCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Coach> specification = createSpecification(criteria);
        return coachRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Coach} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Coach> findByCriteria(CoachCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Coach> specification = createSpecification(criteria);
        return coachRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CoachCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Coach> specification = createSpecification(criteria);
        return coachRepository.count(specification);
    }

    /**
     * Function to convert {@link CoachCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Coach> createSpecification(CoachCriteria criteria) {
        Specification<Coach> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Coach_.id));
            }
            if (criteria.getNom() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNom(), Coach_.nom));
            }
            if (criteria.getPrenom() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPrenom(), Coach_.prenom));
            }
            if (criteria.getAncienete() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAncienete(), Coach_.ancienete));
            }
        }
        return specification;
    }
}
