package sn.federation.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import sn.federation.domain.Joueur;
import sn.federation.domain.*; // for static metamodels
import sn.federation.repository.JoueurRepository;
import sn.federation.service.dto.JoueurCriteria;

/**
 * Service for executing complex queries for {@link Joueur} entities in the database.
 * The main input is a {@link JoueurCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Joueur} or a {@link Page} of {@link Joueur} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class JoueurQueryService extends QueryService<Joueur> {

    private final Logger log = LoggerFactory.getLogger(JoueurQueryService.class);

    private final JoueurRepository joueurRepository;

    public JoueurQueryService(JoueurRepository joueurRepository) {
        this.joueurRepository = joueurRepository;
    }

    /**
     * Return a {@link List} of {@link Joueur} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Joueur> findByCriteria(JoueurCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Joueur> specification = createSpecification(criteria);
        return joueurRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Joueur} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Joueur> findByCriteria(JoueurCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Joueur> specification = createSpecification(criteria);
        return joueurRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(JoueurCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Joueur> specification = createSpecification(criteria);
        return joueurRepository.count(specification);
    }

    /**
     * Function to convert {@link JoueurCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Joueur> createSpecification(JoueurCriteria criteria) {
        Specification<Joueur> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Joueur_.id));
            }
            if (criteria.getNom() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNom(), Joueur_.nom));
            }
            if (criteria.getPrenom() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPrenom(), Joueur_.prenom));
            }
            if (criteria.getAge() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAge(), Joueur_.age));
            }
            if (criteria.getPoste() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPoste(), Joueur_.poste));
            }
            if (criteria.getEquipeId() != null) {
                specification = specification.and(buildSpecification(criteria.getEquipeId(),
                    root -> root.join(Joueur_.equipe, JoinType.LEFT).get(Equipe_.id)));
            }
        }
        return specification;
    }
}
