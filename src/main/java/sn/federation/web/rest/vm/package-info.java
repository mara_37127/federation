/**
 * View Models used by Spring MVC REST controllers.
 */
package sn.federation.web.rest.vm;
